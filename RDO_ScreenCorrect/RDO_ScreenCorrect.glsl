//*****************************************************************************/
// 
// Filename: RDO_Screencorrect
//
// Copyright (c) 2016 RodeoFX
// All rights reserved.
// 
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of RodeoFX
// and are protected under applicable copyright and trade secret law.
// They may not be disclosed to, copied or used by any third party without
// the prior written consent of RodeoFX.
//*****************************************************************************/
// SC shader sjacob
// v001
// 

#version 120
uniform sampler2D front, cleanplate;
uniform float adsk_result_w, adsk_result_h;
uniform vec3 pick;
uniform int color_math;

void main()
{
    //The next line is required for the shader to work. It tells the shader which pixel we're now working on.
   vec2 uv = gl_FragCoord.xy / vec2( adsk_result_w, adsk_result_h);
   //Get our inputs (notice it refers to the previous line "uv" to indicate which pixel we're looking at)
   vec3 B = texture2D(front, uv).rgb;
   vec3 A = texture2D(cleanplate, uv).rgb;
   float rmatte = (B.r - max (B.g,B.b))/ (A.r - max (A.g,A.b));
   float gmatte = (B.g - max (B.r,B.b))/ (A.g - max (A.r,A.b));
   float bmatte = (B.b - max(B.r,B.g)) / (A.b - max(A.r,A.g));
//SC results
//Green screen
   float scg_red = B.r - clamp(gmatte,0,1) * (A.r - pick.r);
   float scg_green = B.g - clamp(gmatte,0,1) * (A.g - pick.g);
   float scg_blue = B.b - clamp(gmatte,0,1) * (A.b - pick.b);

//Blue screen
   float scb_red = B.r - clamp(bmatte,0,1) * (A.r - pick.r);
   float scb_green = B.g - clamp(bmatte,0,1) * (A.g - pick.g);
   float scb_blue = B.b - clamp(bmatte,0,1) * (A.b - pick.b);

//Red screen
   float scr_red = B.r - clamp(rmatte,0,1) * (A.r - pick.r);
   float scr_green = B.r - clamp(rmatte,0,1) * (A.g - pick.g);
   float scr_blue = B.r - clamp(rmatte,0,1) * (A.b - pick.b);

   if( color_math ==0)        
      gl_FragColor.rgb = vec3(scg_red,scg_green,scg_blue);
   if( color_math ==1)
      gl_FragColor.rgb = vec3(scb_red,scb_green,scb_blue);
   if( color_math ==2)
      gl_FragColor.rgb = vec3(scr_red,scr_green,scr_blue);
}
