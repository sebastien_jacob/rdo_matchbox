//*****************************************************************************/
// 
// Filename: RDO_LogicOps
//
// Copyright (c) 2016 RodeoFX
// All rights reserved.
// 
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of RodeoFX
// and are protected under applicable copyright and trade secret law.
// They may not be disclosed to, copied or used by any third party without
// the prior written consent of RodeoFX or Sebastien Jacob.
//*****************************************************************************/
// Shader sjacob
// v001
//

uniform sampler2D input1, input2, input3;
uniform float adsk_result_w, adsk_result_h;
uniform int blendMode;
uniform float amount;

vec4 adsk_getBlendedValue( int blendType, vec4 srcColor, vec4 dstColor );

vec3 fade(vec3 fg, vec3 bg, float blend_amount)
{
	return vec3(mix(bg, fg, blend_amount));
}

vec3 comp(vec3 fg, vec3 bg, vec3 alpha)
{
	return vec3(alpha * fg + (1.0 - alpha) * bg);
}

void main()
{
   vec2 coords = gl_FragCoord.xy / vec2( adsk_result_w, adsk_result_h );
   vec3 f = texture2D(input1, coords).rgb;
   vec3 b = texture2D(input2, coords).rgb;
   vec3 m = texture2D(input3, coords).rgb;

   vec4 blendedColor = adsk_getBlendedValue(
      blendMode, vec4( f, 1.0 ), vec4( b, 1.0 )
   );

   vec3 blend_result = comp(f, b, m);
   vec3 blended_result = comp(blendedColor.rgb, b ,m);

	//Output the final result.
	if (blendMode == 99) {
		//Had to add a special case for straight out blend
		gl_FragColor = vec4((fade((blend_result), b, amount)), m);
	} else {
		//else Output the result
		gl_FragColor = vec4((fade(blended_result, b, amount)), m);
	}
}
