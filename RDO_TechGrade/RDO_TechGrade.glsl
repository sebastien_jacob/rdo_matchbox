//*****************************************************************************/
// 
// Filename: RDO_TechGrade 
//
// Copyright (c) 2016 RodeoFX
// All rights reserved.
// 
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of RodeoFX
// and are protected under applicable copyright and trade secret law.
// They may not be disclosed to, copied or used by any third party without
// the prior written consent of RodeoFX.
//*****************************************************************************/
// Techgrade shader sjacob
// v002 : merci laurent pour le cue sur l'invert
// v003 : Upgrade de code pour tenir compte du matte input pour compatibilite action/timeline.

#version 120
uniform sampler2D front, matte;
uniform float adsk_result_w, adsk_result_h;
uniform vec3 picker;
uniform float temperature;
uniform float tint;
uniform float gain;
uniform float offset;
uniform bool invert;
uniform vec3 slope_result;

//Completely disregarding the notion of power in the  ASC CDL for our purposes, everything must stay linear. 
float power = 1.0;
void main()
{
   vec2 uv = gl_FragCoord.xy / vec2( adsk_result_w, adsk_result_h );
   vec3 color = texture2D(front, uv).rgb;
   vec3 m = texture2D(matte, uv).rgb;
   float nr = picker.g / picker.r;
   float ng = picker.g / picker.g;
   float nb = picker.g / picker.b;
   vec3 slope = vec3((( 1.0 + gain ) * nr + temperature + tint), (( 1.0 + gain ) * ng - tint), (( 1.0 + gain ) * nb - temperature + tint));

   float forward_red = pow ((color.r * slope.r + offset), power);
   float forward_green = pow ((color.g * slope.g + offset), power);
   float forward_blue = pow ((color.b * slope.b + offset), power);
   vec3 result_forward = vec3(forward_red, forward_green, forward_blue);

   float inverse_red = (color.r - offset) * 1 / slope.r;
   float inverse_green = (color.g - offset) * 1 / slope.g;
   float inverse_blue = (color.b - offset) * 1 / slope.b;
   vec3 result_inverse = vec3(inverse_red, inverse_green, inverse_blue);

   if( invert ==false)        
      gl_FragColor = vec4(result_forward, m);
   else
      gl_FragColor = vec4(result_inverse, m);
}
