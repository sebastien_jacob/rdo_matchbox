//*****************************************************************************/
// 
// Filename: RDO_Negative_Rolloff 
//
// Copyright (c) 2016 RodeoFX
// All rights reserved.
// 
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of RodeoFX
// and are protected under applicable copyright and trade secret law.
// They may not be disclosed to, copied or used by any third party without
// the prior written consent of RodeoFX.
//*****************************************************************************/
// The intent of this shader is to remap negative values into positive, reserved space
//
// v001 - Added the safetynet clamp like in the Nuke version.

#version 120
#define pi 3.1415926535897932384626433832795

uniform sampler2D front;
uniform float adsk_result_w, adsk_result_h;
uniform int direction;
uniform float floor;

void main()
{
   vec2 coords = gl_FragCoord.xy / vec2( adsk_result_w, adsk_result_h );
   vec3 color_add = texture2D(front, coords).rgb;
   vec3 color_remove = texture2D(front, coords).rgb;
   float add_red = (color_add.r < floor ? floor * color_add.r / (1.0 + floor ) + floor / ( 1 + floor) : color_add.r);
   float add_green = (color_add.g < floor ? floor * color_add.g / (1.0 + floor ) + floor / ( 1 + floor) : color_add.g);
   float add_blue = (color_add.b < floor ? floor * color_add.b / (1.0 + floor ) + floor / ( 1 + floor) : color_add.b);
   float rem_red = (color_remove.r < floor ? color_remove.r * ( 1.0 + floor) / floor - 1.0 : color_remove.r);
   float rem_green = (color_remove.g < floor ? color_remove.g * ( 1.0 + floor) / floor - 1.0 : color_remove.g);
   float rem_blue = (color_remove.b < floor ? color_remove.b * ( 1.0 + floor) / floor - 1.0 : color_remove.b);

   if( direction ==0)        
      gl_FragColor.rgb = vec3(add_red, add_green, add_blue);
   else
      gl_FragColor.rgb = vec3(rem_red, rem_green, rem_blue);
}


