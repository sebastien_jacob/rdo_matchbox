//*****************************************************************************/
// 
// Filename: RDO_Softclip 
//
// Copyright (c) 2016 RodeoFX
// All rights reserved.
// 
// This computer source code and related instructions and comments are the
// unpublished confidential and proprietary information of RodeoFX
// and are protected under applicable copyright and trade secret law.
// They may not be disclosed to, copied or used by any third party without
// the prior written consent of RodeoFX.
//*****************************************************************************/
// The intent of this shader is to 'as losslessly as possible' remap large floating point values to a 0-1 range and back again
// SJacob
// v001 - Combined both previous shaders into one.
// v002 - Added the safetynet clamp like in the Nuke version.
// v003 - Cleaned up safetynet code, bumped the output to rgba for action (thankls DJ)

#version 120
#define pi 3.1415926535897932384626433832795

uniform sampler2D front, matte;
uniform float adsk_result_w, adsk_result_h;
uniform int direction;

void main()
{
   vec2 uv = gl_FragCoord.xy / vec2( adsk_result_w, adsk_result_h );
   vec3 color_add = texture2D(front, uv).rgb;
   vec3 color_remove = texture2D(front, uv).rgb;
   vec3 m = texture2D(matte, uv).rgb;
   float add_red = atan (0.5 * pi * color_add.r) / (0.5 * pi);
   float add_green = atan (0.5 * pi * color_add.g) / (0.5 * pi);
   float add_blue = atan (0.5 * pi * color_add.b) / (0.5 * pi);
   float rem_red = tan ( clamp ( color_remove.r, 0 ,0.99999 ) / ( 2 / pi )) / ( pi / 2 );
   float rem_green = tan ( clamp ( color_remove.g, 0 ,0.99999 ) / ( 2 / pi )) / ( pi / 2 );
   float rem_blue = tan ( clamp ( color_remove.b, 0 ,0.99999 ) / ( 2 / pi )) / ( pi / 2 );
   vec3 result_add = vec3(add_red, add_green, add_blue);
   vec3 result_remove = vec3(rem_red, rem_green, rem_blue);

   if( direction ==0)        
      gl_FragColor = vec4(result_add, m);
   else
      gl_FragColor = vec4(result_remove, m);
}


